package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will used in handling http request.
@RestController
//will require all routes withing the class to use the set endpoint as part of its route.

@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
	// Intro to Spring Boot Activity
	//Initialize ArrayList called enrolles
	ArrayList<String> enrolles= new ArrayList<>();
	//Create /enroll route
	@GetMapping("/enroll")
	public String getEnrolle(@RequestParam(value="name", defaultValue = "John Doe")String name){
		enrolles.add(name);
		return String.format("Welcome %s!", name);
	}
	//Create /getEnrolles route which returns the enrolle ArrayList as a string.
	@GetMapping("/getEnrolles")
	public String enrolleStudents(){
		return String.format(enrolles.toString());
	}
	//Create /nameage route which has multiple query string parameters (name, age)
	@GetMapping("/nameage")
	public String getAge(@RequestParam(value="name",defaultValue = "John Doe")String name, @RequestParam(value="age")int age){
		return String.format("Hello %s! My age is %d", name, age);
	}
	//Create /courses/id dynamic route using a path variable of id
	@GetMapping("/courses/{id}")
	public String getCourse(@PathVariable("id")String id){
		if(id.equalsIgnoreCase("java101")){
			return	String.format("Java 101, MWF 8:00AM-11:00 AM, Price: PHP 3000.00");
		}
		else if(id.equalsIgnoreCase("sql101")){
			return	String.format("SQL 101, TTH 1:00PM-4:00 PM, Price: PHP 2000.00");
		}
		else if(id.equalsIgnoreCase("javaee101")){
			return	String.format("Java EE 101, MWF 1:00PM-4:00 PM, Price: PHP 3500.00");
		}
		else
			return String.format("The course cannot be found.");
	}

	// Asynchronous Activity
	@GetMapping("/welcome")
	public String getRole(@RequestParam(value="user",defaultValue = "Joe")String user, @RequestParam(value="role")String role){

		if(role.equalsIgnoreCase("admin")||role.equalsIgnoreCase("Admin")){
			return String.format("Welcome back to the class portal,  %s %s!",role, user);
		}
		else if(role.equalsIgnoreCase("teacher")||role.equalsIgnoreCase("Teacher")){
			return String.format("Thank you for logging in, %s %s!",role, user);
		}
		else if(role.equalsIgnoreCase("student")||role.equalsIgnoreCase("Student")){
			return String.format("Welcome to the class portal, %s!", user);
		}
		else{
			return String.format("Role out of range!");
		}
	}
	ArrayList<Student> students= new ArrayList<>();
	@GetMapping("/register")
	public String student(@RequestParam(value="id")int id,@RequestParam(value="name", defaultValue = "Joe")String name, @RequestParam(value="course",defaultValue = "none")String course ){
		Student student=new Student(id,name,course);
		students.add(student);
		return String.format("%d your id number is registered on the system!", id);
	}
	@GetMapping("/account")
	public String account(){


		return String.format(students.toString());
	}
	@GetMapping("/account/{id}")
	public String getAccount(@PathVariable("id") int id){

		for(int i=0;i<students.size();i++){
			if(students.get(i).getId()==id){
				return String.format("Welcome back %s! You are currently enrolled in %s", students.get(i).getName(), students.get(i).getCourse());
			}

		}
		return String.format("Your provide %d is not found in the system!", id);
	}





	//	http://localhost:8080/hello
	@GetMapping("/hello")
	//Maps get request to route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}
	//Route with String Query
	// http://localhost:8080/hi?name=value
	@GetMapping("/hi")
	//@ReqeustParam annotation that allows us to extract data from
	public String hi(@RequestParam(value="name",defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}
	// Multiple Parameters
	// localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name",defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane")String friend){
		return String.format("Hello %s! My name is %s.", friend,name);
	}
	//Route with path variables
	//Dynamic Data is obtained directly from the url
	//localhost:8080/name
	@GetMapping("/hello/{name}")
	//"@PathVariable" annotation allows us to extract data directly from the url
	public String greatFriend(@PathVariable("name")String name){
		return String.format("Nice to meet you %s!",name);
	}





	}
